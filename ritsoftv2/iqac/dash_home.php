<?php
include("includes/header.php");
include("includes/sidenav.php");

?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><span style="font-weight:bold;">WELCOME IQAC HOME
            </span></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
    
    <div class="row">
        <div class="col-lg-3 col-md-8">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-table fa-fw fa-2x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">
                                  
                            </div>
                            <div>Create Sample Structure</div>
                        </div>
                    </div>
                </div>
                <a href="samplestructure.php">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        
        <div class="row">
        <div class="col-lg-3 col-md-8">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-table fa-fw fa-2x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">
                                  
                            </div>
                            <div>folder Source to destination</div>
                        </div>
                    </div>
                </div>
                <a href="sampledestin.php">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div> 
           <!-- /.panel-body -->

        <!-- /.panel-footer -->
    </div>
    <!-- /.panel .chat-panel -->
</div>

<?php

include("includes/footer.php");
?>
