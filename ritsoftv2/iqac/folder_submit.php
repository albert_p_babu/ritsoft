<?php
require __DIR__ . '/vendor/autoload.php';
include("includes/header.php");
include("includes/sidenav.php");
/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Drive API PHP Quickstart');
    $client->setScopes(Google_Service_Drive::DRIVE_METADATA_READONLY);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}



        if((!empty($_POST['fname'])) && (empty($_POST['parentfolder']))){
            //get client object
            $client = getClient();
            $service = new \Google_Service_Drive($client);
            $folder = new \Google_Service_Drive_DriveFile();
            /**
            *  Initilize drive services object
            */
            $folderName=$_POST['fname'];
            
            $folder->setName($folderName);
            $folder->setMimeType('application/vnd.google-apps.folder');

            $file =$service->files->create($folder);
     
            if(!empty($file)){?>
            <script type="text/javascript"> 
                alert("folder added successfully");location.replace("new_folder.php");
            </script><?php }
            else
                {?>
            <script type="text/javascript"> 
                alert("Try Again..!");location.replace("new_folder.php");
            </script><?php }
        }
        if((!empty($_POST['fname'])) && (!empty($_POST['parentfolder']))){

             $client = getClient();
            $service = new \Google_Service_Drive($client);
            
            $parentname=$_POST['parentfolder'];
            $folderName=$_POST['fname'];
           
             $optParams = new \Google_Service_Drive_DriveFile(array(
              'name' => $parentname,
               'mimeType' => 'application/vnd.google-apps.folder',
              'fields' => 'nextPageToken, files(id, name)'
            ));
            $results = $service->files->listFiles($optParams);

            foreach ($results as $file){
            if($file['name']==$parentname){
                $parentId=$file['id'];
                //printf("its here",$parentId);
            }
        }


            // Setting File Matadata
            $fileMetadata = new \Google_Service_Drive_DriveFile(array(
            'name' => $folderName,
            'parents' => array($parentId),
            'mimeType' => 'application/vnd.google-apps.folder'));
        
            // Creating Folder with given Matadata and asking for ID field as result
            $folder = $service->files->create($fileMetadata, array('fields' => 'id'));
        ?>
            <script type="text/javascript"> 
                alert("folder added successfully");location.replace("new_folder.php");
            </script><?php
        
        
             
        }
