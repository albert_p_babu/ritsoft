<?php
require __DIR__ . '/vendor/autoload.php';

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Drive API PHP Quickstart');
    $client->setScopes(Google_Service_Drive::DRIVE_METADATA_READONLY);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;

}

$client = getClient();
$service = new \Google_Service_Drive($client);
$folder = new \Google_Service_Drive_DriveFile();
           
$folderName='new fol';
            
$folder->setName($folderName);
  $folder->setMimeType('application/vnd.google-apps.folder');
 $file =$service->files->create($folder);
 $parent=$file->getId();
$search='';
$parentId='12MQMrC0jIRuHPcslChQ-lytCcTw0emek';
$type = 'application/vnd.google-apps.folder';
$query = '';

        // Checking if search is empty the use 'contains' condition if search is empty (to get all files or folders).
        // Otherwise use '='  condition
        $condition = $search!=''?'=':'contains';
        
        // Search all files and folders otherwise search in root or  any folder
        $query .= $parentId!='all'?"'".$parentId."' in parents":"";
        
        // Check if want to search files or folders or both
        switch ($type) {
            case "files":
                $query .= $query!=''?' and ':'';
                $query .= "mimeType != 'application/vnd.google-apps.folder' 
                            and name ".$condition." '".$search."'";
                break;

            case "folders":
                $query .= $query!=''?' and ':'';
                $query .= "mimeType = 'application/vnd.google-apps.folder' and name contains '".$search."'";
                break;
            default:
                $query .= "";
                break;
        }

        // Make sure that not list trashed files
        $query .= $query!=''?' and trashed = false':'trashed = false';
        $optParams = array('q' => $query,'pageSize' => 1000);

        // Returns the list of files and folders as object
        $results = $service->files->listFiles($optParams);

        //echo $results;

        // Return false if nothing is found
        if (count($results->getFiles()) == 0) {
            return array();
        }

        // Converting array to object
        foreach ($results->getFiles() as $file) {
            $parameters = [
            'q' => "'$folderId' in parents",
            'fields' => 'files(id, name)'];
            $result = $service->files->listFiles($parameters);
            foreach ($result->getFiles() as $subfile){
            if($file['mimeType']!='application/vnd.google-apps.folder'){
                
              $newfolder=$file->getName();
              $fileMetadata = new \Google_Service_Drive_DriveFile(array(
            'name' => $newfolder,
            'parents' => array($parent),
            'mimeType' => 'application/vnd.google-apps.folder'));
        
            // Creating Folder with given Matadata and asking for ID field as result
            $newfolders = $service->files->create($fileMetadata, array('fields' => 'id'));
        }
        else if($file['mimeType']=='application/vnd.google-apps.folder'){

            $parameters = [
            'q' => "'$folderId' in parents",
            'fields' => 'files(id, name)'];
            $result = $service->files->listFiles($parameters);

        }
            
        }